# theNoy portfolio

## Live demo
https://the-noy.com/

## Runing localy

### Dependencies

In order to use this setup you need to have installed the following dependencies:

1.  Node - min v8.15.0
2.  NPM - min v5.6.0
    or
3.  Yarn - min v1.3.2
4.  Bash terminal (Default on OSX/Linux, GitBash or similar on Windows)


### Install

```sh
yarn

# or

npm i
```

### Run

```sh
yarn start

# or

npm start
```