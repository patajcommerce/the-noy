interface Art {
    id: number;
    title: string;
    imageName: string;
    subdesc?: boolean;
    creationDate: string;
    children?: Art[];
    isSquere?: boolean;
    facebook?: string;
    instagram?: string;
    pinterest?: string;
    youtube?: string;
    additionalLink?: string;
}

const artList: Art[] = [
    {
        id: 0,
        title: 'Birae team',
        imageName: 'team-birae.jpg',
        creationDate: 'Sep 2019',
        isSquere: true
    },
    {
        id: 1,
        title: 'World Testers',
        imageName: 'wolrd-testers.jpg',
        creationDate: 'Nov 2019'
    },
    {
        id: 2,
        title: 'Yelven',
        imageName: 'yelven.png',
        creationDate: 'May 2020'
    },
    {
        id: 3,
        subdesc: true,
        title: 'House Mouse',
        instagram: 'https://www.instagram.com/p/CMnW0FNBkNN/',
        imageName: 'house-mouse.png',
        creationDate: 'Dec 2020'
    },
    {
        id: 4,
        title: 'Lia Moonlight',
        imageName: 'lia.png',
        instagram: 'https://www.instagram.com/p/CMEo9q4htHf/',
        creationDate: 'Jan 2021',
        children: [
            {
                id: 4.1,
                title: 'Lia Moonlight',
                imageName: 'lia-flat.png',
                creationDate: 'Jan 2021',
            }
        ]
    },
    {
        id: 5,
        title: 'Moon',
        imageName: 'moon.png',
        creationDate: 'Jan 2021',
        additionalLink: "https://www.youtube.com/user/kara2509"
    },
    {
        id: 6,
        title: "Geisha's lights",
        imageName: 'geisha.png',
        instagram: 'https://www.instagram.com/p/CMPA90nhv5w/',
        creationDate: 'Jan 2021',
        children: [
            {
                id: 6.1,
                title: 'Geisha',
                imageName: 'geisha-flat.png',
                creationDate: 'Jan 2021'
            },
            {
                id: 6.2,
                title: 'Geisha',
                imageName: 'geisha-original.png',
                creationDate: 'Jan 2021'
            }
        ]
    },
    {
        id: 7,
        title: "Duncan Dracohorn",
        imageName: 'duncan.png',
        instagram: 'https://www.instagram.com/p/CMwKvQwB5HI/',
        creationDate: 'Jan 2021',
        children: [
            {
                id: 7.1,
                title: 'Duncan',
                imageName: 'duncan-flat.png',
                creationDate: 'Jan 2021'
            }
        ]
    },
    {
        id: 8,
        title: "The Noy",
        imageName: 'the-noy.png',
        instagram: 'https://www.instagram.com/p/CL9M1rkB7bn/',
        creationDate: 'Feb 2021',
        children: [
            {
                id: 8.1,
                title: 'the Noy',
                imageName: 'the-noy-flat.png',
                creationDate: 'Feb 2021',
            }
        ]
    },
    {
        id: 9,
        title: "Plant - DTIYS",
        imageName: 'dtiys-plant.png',
        additionalLink: 'https://www.instagram.com/agnesillustrates/',
        instagram: 'https://www.instagram.com/p/CL_f3lJhKy_/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 9.1,
                title: 'Elf',
                imageName: 'dtiys-plant-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 10,
        title: "Mermaid - DTIYS",
        imageName: 'dtiys-mermaid.png',
        instagram: 'https://www.instagram.com/p/CMA-6iuBX4T/',
        additionalLink: 'https://www.instagram.com/leffiesart/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 10.1,
                title: 'Mermaid',
                imageName: 'dtiys-mermaid-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 11,
        title: "Star - DTIYS",
        imageName: 'dtiys-star.png',
        instagram: 'https://www.instagram.com/p/CMJ6Th0hEig/',
        additionalLink: 'https://www.instagram.com/kaiselka/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 11.1,
                title: 'Elf',
                imageName: 'dtiys-str-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 12,
        title: "Scarlett Witch",
        imageName: 'vanda.png',
        instagram: 'https://www.instagram.com/p/CMUvBW6BVVM/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 12.1,
                title: 'Wanda',
                imageName: 'vanda-full.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 13,
        title: "Magic - DTIYS",
        imageName: 'dtiys-magic.png',
        instagram: 'https://www.instagram.com/p/CMb55othsEk/',
        additionalLink: 'https://www.instagram.com/artofcodaleia/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 13.1,
                title: 'Magic',
                imageName: 'dtiys-magic-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 14,
        title: "Peach - DTIYS",
        imageName: 'dtiys-peach.png',
        instagram: 'https://www.instagram.com/p/CMhfmezBHwX/',
        additionalLink: 'https://www.instagram.com/marinthedrawer/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 14.1,
                title: 'Elf',
                imageName: 'dtiys-peach-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 15,
        title: "Elf Warlock - DTIYS",
        imageName: 'dtiys-elf-warlock.png',
        instagram: 'https://www.instagram.com/p/CMr2_UZBXaM/',
        additionalLink: 'https://www.instagram.com/artofrubensanchez/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 15.1,
                title: 'Elf',
                imageName: 'dtiys-elf-warlock-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 16,
        title: "Rhaelkur",
        imageName: 'rhaelkur.png',
        instagram: 'https://www.instagram.com/p/CNZefYLB0pe/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 16.1,
                title: 'Elf',
                imageName: 'rhaelkur-flat.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 17,
        title: "Anyia",
        imageName: 'anyia.png',
        instagram: 'https://www.instagram.com/p/CM4cQHGhF-p/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 17.1,
                title: 'Anyia',
                imageName: 'anyia-flat.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 18,
        title: "Zed",
        imageName: 'zed.png',
        instagram: 'https://www.instagram.com/p/CM_42ZwhB36/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 18.1,
                title: 'Elf',
                imageName: 'zed-flat.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 19,
        title: "Rena",
        imageName: 'rena.png',
        instagram: 'https://www.instagram.com/p/CNMl7xghXJX/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 19.1,
                title: 'Rena',
                imageName: 'rena-flat.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 20,
        title: "Green and Violet - DTIYS",
        imageName: 'dtiys-green.png',
        instagram: 'https://www.instagram.com/p/CM0quQSBN9b/',
        additionalLink: 'https://www.instagram.com/thewbs_art/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 20.1,
                title: 'Elf',
                imageName: 'dtiys-green-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 21,
        title: "Octavia",
        imageName: 'octavia.png',
        instagram: 'https://www.instagram.com/p/CNUKJQ5hd09/',
        creationDate: 'Mar 2021'
    },
    {
        id: 22,
        title: "Elf child",
        imageName: 'dtiys-child-elf.png',
        instagram: 'https://www.instagram.com/p/CNFuSbqhIdU/',
        additionalLink: 'https://www.instagram.com/milvilla_/',
        creationDate: 'Mar 2021',
        children: [
            {
                id: 22.1,
                title: 'Elf',
                imageName: 'dtiys-child-elf-original.png',
                creationDate: 'Mar 2021',
            }
        ]
    },
    {
        id: 23,
        title: "Let it snow",
        imageName: 'snow.png',
        creationDate: 'Apr 2021'
    },
    {
        id: 24,
        title: "Magic water - DTIYS",
        imageName: 'dtiys-magic-water.png',
        instagram: 'https://www.instagram.com/p/CNSGGOGB7Vo/',
        additionalLink: 'https://www.instagram.com/paperdollavila/',
        creationDate: 'Apr 2021',
        children: [
            {
                id: 24.1,
                title: 'Magic water',
                imageName: 'dtiys-water-original.png',
                creationDate: 'Apr 2021',
            }
        ]
    }
]

export default artList;
