import * as React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { AnimatedSwitch } from 'react-router-transition';

import * as Loadables from './loadables';
import { Routes } from '@utilities';

import './app.scss';

export const App = () => (
	<Router>
		<AnimatedSwitch
			atEnter={{ opacity: 0 }}
			atLeave={{ opacity: 0 }}
			atActive={{ opacity: 1 }}
			className="switch-wrapper"
		>
			<Route path={Routes.BASE} component={Loadables.Home} exact />
			<Route path={Routes.ABOUT} component={Loadables.About} />
			<Route
				path={Routes.GALLERY}
				render={({ match: { url } }) => (
					<AnimatedSwitch
						atEnter={{ opacity: 0 }}
						atLeave={{ opacity: 0 }}
						atActive={{ opacity: 1 }}
						className="switch-wrapper"
					>
						<Route path={`${url}${Routes.BASE}`} component={Loadables.Galllery} exact />
						<Route path={`${url}${Routes.BASE}:id`} component={Loadables.Single} />
					
					</AnimatedSwitch>
				)}
			/>
			<Route component={Loadables.NotFound} />
		</AnimatedSwitch>
	</Router>
);
