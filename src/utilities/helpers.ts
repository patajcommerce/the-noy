export const saveLocale = (locale: string): void => localStorage.setItem('locale', locale);
export const setDocumentLang = (locale: string): void => document.documentElement.setAttribute('lang', locale);
