export enum Routes {
	BASE = '/',
	ABOUT = '/about',
	GALLERY = '/gallery',
}
