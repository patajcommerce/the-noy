import { AuthActionType } from './enums';
import { AuthState, AuthAction } from './interfaces';

export const initialState: AuthState = {
	locale: 'en',
	loading: false
};

export default (state = initialState, { type, payload }: AuthAction): AuthState => {
	switch (type) {
		case AuthActionType.SET_LOCALE_REQUEST:
			return {
				...state,
				loading: true
			};
		case AuthActionType.SET_LOCALE_FAILED:
			return {
				...state,
				...payload,
				loading: false
			};
		case AuthActionType.SET_LOCALE_SUCCESS:
			return {
				...state,
				...payload,
				loading: false
			};
		case AuthActionType.RESET_AUTH:
			return initialState;
		default:
			return state;
	}
};
