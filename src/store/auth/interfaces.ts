import { AuthActionType } from './enums';

export interface AuthState {
	locale: string;
	[x: string]: any;
}

export interface AuthAction {
	type: AuthActionType;
	payload?: Partial<AuthState>;
}
