import { put, takeLatest, CallEffect, PutEffect, ForkEffect } from 'redux-saga/effects';

import { i18n } from '@i18n';
import { AuthAction } from './interfaces';
import { AuthActionType } from './enums';
import { saveLocale, setDocumentLang } from '@utilities';

type AuthSafaEffect = Generator<CallEffect<any> | PutEffect<AuthAction>>;
type AuthSagaForkEffect = Generator<ForkEffect<void>>;

export function* localeEffect(action: AuthAction): AuthSafaEffect {
	try {
		const locale = action.payload?.locale;

		if (!locale) {
			yield put({
				type: AuthActionType.SET_LOCALE_FAILED
			});

			return;
		}

		i18n.changeLanguage(locale);
		saveLocale(locale);
		setDocumentLang(locale);

		yield put({
			type: AuthActionType.SET_LOCALE_SUCCESS,
			payload: { locale }
		});
	} catch (error) {
		yield put({
			type: AuthActionType.SET_LOCALE_FAILED
		});
	}
}

export function* localeSaga(): AuthSagaForkEffect {
	yield takeLatest(AuthActionType.SET_LOCALE_REQUEST, localeEffect);
}
