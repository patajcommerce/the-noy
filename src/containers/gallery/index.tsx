import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

import { Wrapper } from '@components';
import artList from '../../assets/arts';

import './index.scss';

export const Gallery: React.FunctionComponent = () => {
	const { t } = useTranslation();

	document.addEventListener("DOMContentLoaded", function() {
		let lazyloadImages: NodeListOf<Element>;    
	  
		if ("IntersectionObserver" in window) {
		  lazyloadImages = document.querySelectorAll(".lazy");
		  var imageObserver = new IntersectionObserver(function(entries, observer) {
			entries.forEach(function(entry) {
			  if (entry.isIntersecting) {
				var image = entry.target;
				image.classList.remove("lazy");
				imageObserver.unobserve(image);
			  }
			});
		  });
	  
		  lazyloadImages.forEach((image) => {
			imageObserver.observe(image);
		  });
		} else {  
		  let lazyloadThrottleTimeout: any;
		  lazyloadImages = document.querySelectorAll(".lazy");
		  
		  const lazyload = () => {
			if(lazyloadThrottleTimeout) {
			  clearTimeout(lazyloadThrottleTimeout);
			}    
	  
			lazyloadThrottleTimeout = setTimeout(() => {
			  var scrollTop = window.pageYOffset;
			  lazyloadImages.forEach((img: any) => {
				  if(img.offsetTop < (window.innerHeight + scrollTop)) {
					img.src = img.dataset.src;
					img.classList.remove('lazy');
				  }
			  });
			  if(lazyloadImages.length == 0) { 
				document.removeEventListener("scroll", lazyload);
				window.removeEventListener("resize", lazyload);
				window.removeEventListener("orientationChange", lazyload);
			  }
			}, 20);
		  }
	  
		  document.addEventListener("scroll", lazyload);
		  window.addEventListener("resize", lazyload);
		  window.addEventListener("orientationChange", lazyload);
		}
	  })

	return <Wrapper>
		<div className="gallery">
			{artList.reverse().map(art => (
				<NavLink key={art.id} to={`/gallery/${art.id}`} className="gallery__item">

					<div className="gallery__item__image lazy" style={{backgroundImage: `url(assets/images/${art.imageName})`}}/>

					<div className="gallery__item__content">
						<div>
							<p className="gallery__item__content__view">{t('view details of')}</p>
							<p className="gallery__item__content__title">{t(art.title)}</p>
							<p className="gallery__item__content__date">{art.creationDate}</p>
						</div>
					</div>

				</NavLink>
			))}
		</div>
	</Wrapper>;
};

export default Gallery;
