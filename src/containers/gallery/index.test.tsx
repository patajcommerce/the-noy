import * as React from 'react';
import { shallow } from 'enzyme';

import { Gallery } from '.';

describe('Gallery component', () => {
	it('should render successfully', () => {
		const tree = shallow(<Gallery />);

		expect(tree).toMatchSnapshot();
	});
});
