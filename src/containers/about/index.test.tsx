import * as React from 'react';
import { shallow } from 'enzyme';

import { About } from '.';

describe('About component', () => {
	it('should render successfully', () => {
		const tree = shallow(<About />);

		expect(tree).toMatchSnapshot();
	});
});
