import * as React from 'react';
import { useTranslation } from 'react-i18next';

import { Wrapper, Tooltip } from '@components';
import { faEnvelope, faHeart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './index.scss';

export const About: React.FunctionComponent = () => {
	const { t } = useTranslation();
	const isMobile = window.innerWidth < 1025;

	return <Wrapper>
		<div className="about">
			{isMobile && <div className="about__image" />}

			<div className="about__content">
				<p className="about__content__title">
					{t('About me')}
					<Tooltip tooltipText={<span className="about__content__tooltip__text" >{t('Contact me')}<FontAwesomeIcon icon={faHeart} /></span>}>
						<a href="mailto: paulinatargo@gmail.com" target="_blank"><FontAwesomeIcon icon={faEnvelope} /></a>
					</Tooltip>
				</p>

				<p>{t('about-me-text')}</p>
				<p>{t('about-me-text1')}</p>
				<p>{t('about-me-text2')}</p>
				<p>{t('about-me-text3')} <FontAwesomeIcon icon={faHeart} /></p>
			</div>
			
		</div>
	</Wrapper>;
};

export default About;
