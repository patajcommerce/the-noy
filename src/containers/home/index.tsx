import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';
import { Routes } from '@src/utilities';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'

import { Button, Wrapper } from '@components';

import './index.scss';

export const Home: React.FunctionComponent = () => {
	const { t } = useTranslation();
	return (
		<Wrapper isHome>
			<div className="homepage">
				<div className="homepage__content">

					<div className="homepage__logo">
						<img src="../../assets/images/logo.png" />
					</div>

					<NavLink to={Routes.GALLERY} className="homepage__button">
						<Button>
							<div className="homepage__button__content">
								{t('see art gallery')} <FontAwesomeIcon icon={faChevronRight} />
							</div>
						</Button>
					</NavLink>
					
				</div>
			</div>
		</Wrapper>
	);
};

export default Home;
