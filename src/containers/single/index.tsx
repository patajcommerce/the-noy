import * as React from 'react';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink, useParams, useHistory } from 'react-router-dom';
import { Routes } from '@src/utilities';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faPinterest, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faHeart, faChevronRight} from '@fortawesome/free-solid-svg-icons'
import { faArrowAltCircleLeft, faArrowAltCircleRight} from '@fortawesome/free-regular-svg-icons'

import { Wrapper, Tooltip } from '@components';
import artList from '../../assets/arts';

import './index.scss';

export const Single: React.FunctionComponent = () => {
	const { t } = useTranslation();
	const { id }: {id: string} = useParams();
	const history = useHistory();
	const isMobile = window.innerWidth < 1025;

	const art = artList.find(item => item.id === parseInt(id));
	const [prevId, setId] = useState(id);
	const [selectedArt, setSelectedArt] = useState(art);

	
	useEffect(() => {
		if (id !== prevId) {
			setSelectedArt(art);
			setId(id);
		}
	}), [id];

	if (!art) {
		history.push(`/gallery`)
		return null;
	}

	const sideImageList = art?.children || [];
	if (!sideImageList.find(item => item.id === art.id)) {
		sideImageList.unshift(art);
	}

	const imageStyle = {
		backgroundImage: `url('../../assets/images/${selectedArt?.imageName}')`, 
		minWidth: isMobile && '50%' || art?.children && '40vw' || '50vw', width: isMobile && '100%' || art?.children && '40%' || '45%', 
		height: art?.isSquere ? '95vw' : '100vh'
	}


	return <Wrapper>
		<div className="art">
			{art?.children && <div className="art__side">
				{sideImageList.map(subImage => (
					<div key={subImage.id} className={`art__side__image ${selectedArt?.id === subImage.id ? 'art__side__image--active' : ''}`} style={{backgroundImage: `url('../../assets/images/${subImage?.imageName}')`}} onClick={() => setSelectedArt(subImage)} />
				))}
			</div>}

			<div className="art__image" style={imageStyle} />
			
			<div className="art__content">
				<div className="art__content__title">
					<div>
						<NavLink to={`${Routes.GALLERY}/${art.id - 1}`} className={art.id === 0 ? 'avoid-clicks' : ''}>
							<FontAwesomeIcon icon={faArrowAltCircleLeft} />
						</NavLink>

						<span className="art__content__title__image-title">{t(art?.title)}</span>

						<NavLink to={`${Routes.GALLERY}/${art.id + 1}`} className={art.id === artList.length - 1  ? 'avoid-clicks' : ''}>
							<FontAwesomeIcon icon={faArrowAltCircleRight} />
						</NavLink>
					</div>

					<div className="art__content__title__actions">
						{art.facebook && <a href={art.facebook} target="_blank">
							<Tooltip tooltipText={<span className="art__content__tooltip" >{t('share on facebook')}<FontAwesomeIcon icon={faHeart} /></span>}>
								<FontAwesomeIcon icon={faFacebook} />
							</Tooltip>
						</a>}

						{art.pinterest && <a href={art.pinterest} target="_blank">
							<Tooltip tooltipText={<span className="art__content__tooltip" >{t('see on pinterest')}<FontAwesomeIcon icon={faHeart} /></span>}>
								<FontAwesomeIcon icon={faPinterest} />
							</Tooltip>
						</a>}

						{art.instagram && <a href={art.instagram} target="_blank">
							<Tooltip tooltipText={<span className="art__content__tooltip" >{t('like on instagram')}<FontAwesomeIcon icon={faHeart} /></span>}>
								<FontAwesomeIcon icon={faInstagram} />
							</Tooltip>
						</a>}
					</div>
				</div>
				
				<p>{t(`${art?.title}-desc`)}</p>

				{art?.subdesc && <>
					<p className="art__content__info">{t(`${art?.title}-subdesc-title`)}</p>
					<p>{t(`${art?.title}-subdesc`)}</p>
				</>}

				{art?.additionalLink && <Tooltip tooltipText={<span className="art__content__tooltip" >{t(`${art.title}-tooltip`)}<FontAwesomeIcon icon={faHeart} /></span>}>
					<a target="_blank" href={art.additionalLink}><FontAwesomeIcon icon={faChevronRight} /> {t(`${art.title}-link`)}</a>
				</Tooltip>}

				<p className="art__content__info">{t('Image info')}</p>

				<p>{t(`${art?.title}-info`)}</p>
			</div>
		</div>
	</Wrapper>;
};

export default Single;
