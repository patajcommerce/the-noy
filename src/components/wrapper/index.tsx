import * as React from 'react';
import { Header, Footer } from '@components';
import './index.scss';

interface Props {
	readonly children?: React.ReactNode | React.ReactNode[];
	readonly className?: string;
	readonly isHome?: boolean;
}

export const Wrapper: React.FunctionComponent<Props> = ({ children, className, isHome }: Props) => {
	const classes = ['wrapper'];

	if (className) {
		classes.push(className);
	}

	return (
		<div className={classes.join(' ')}>
			<Header isHome={isHome} />
		
			<main className="wrapper__main">{children}</main>

			{!isHome ? <Footer /> : null}
		</div>
	);
};

export default Wrapper;
