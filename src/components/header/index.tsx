import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { Routes } from '@utilities';
import { Button } from '@components';
import { history, RootStore } from '@store/index';
import { AuthState, AuthActionType } from '@store/auth';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faEnvelope, faHeart, faTimes } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faPinterest, faInstagram, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { i18n, locales } from '@i18n';


import './index.scss';
import { useState } from 'react';
import Tooltip from '../tooltip';

interface Props {
	isHome?: boolean
}

export const Header: React.FunctionComponent<Props>= ({isHome}: Props) => {
	const { t } = useTranslation();
	const dispatch = useDispatch();
	const [isOpen, setOpen] = useState(false);

	const currentLang = i18n.language;
	const isMobile = window.innerWidth < 770;

	const onLanguageChange = (locale: string) => {
		dispatch({
			type: AuthActionType.SET_LOCALE_REQUEST,
			payload: { locale }
		});
	};

	const Nav = (): JSX.Element => (
		<nav className="navbar">
			<ul>
				{isMobile && <li>
					<NavLink to={Routes.BASE}>{t('Home')}</NavLink>
				</li>}
				<li>
					<NavLink to={Routes.GALLERY}>{t('Art')}</NavLink>
				</li>
				<li>
					<NavLink to={Routes.ABOUT}>{t('About me')}</NavLink>
				</li>
				<li>
					<Tooltip tooltipText={<span className="navbar__tooltip__text" >{t('Contact me')}<FontAwesomeIcon icon={faHeart} /></span>}>
						<a href="mailto: paulinatargo@gmail.com" target="_blank"><FontAwesomeIcon icon={faEnvelope} />{isMobile ? t('Contact me') : ''}</a>
					</Tooltip>
				</li>
				{/* <li>
					<Tooltip tooltipText={<span className="navbar__tooltip__text" >{t('See me on Facebook')}<FontAwesomeIcon icon={faHeart} /></span>}>
						<a href={'https://www.instagram.com/thenoyart'} target="_blank"><FontAwesomeIcon icon={faFacebook} />{isMobile ? t('Facebook') : ''}</a>
					</Tooltip>
				</li> */}
				<li>
					<Tooltip tooltipText={<span className="navbar__tooltip__text" >{t('See me on Pinterest')}<FontAwesomeIcon icon={faHeart} /></span>}>
						<a href={'https://pl.pinterest.com/theNoyArt/thenoy-digital/'} target="_blank"><FontAwesomeIcon icon={faPinterest} />{isMobile ? t('Pinterest') : ''}</a>
					</Tooltip>
				</li>
				<li>
					<Tooltip tooltipText={<span className="navbar__tooltip__text" >{t('See me on Instagram')}<FontAwesomeIcon icon={faHeart} /></span>}>
						<a href={'https://www.instagram.com/thenoyart'} target="_blank"><FontAwesomeIcon icon={faInstagram} />{isMobile ? t('Instagram') : ''}</a>
					</Tooltip>
				</li>
				{/* <li>
					<Tooltip tooltipText={<span className="navbar__tooltip__text" >{t('See me on youtube')}<FontAwesomeIcon icon={faHeart} /></span>}>
						<a href={'https://www.instagram.com/thenoyart'} target="_blank"	><FontAwesomeIcon icon={faYoutube} />{isMobile ? t('Youtube') : ''}</a>
					</Tooltip>
				</li> */}
				
				{[...locales, 'en'].map((locale: string, index: number) => (
					<li key={`locale_${index}`}>
						<button 
							className={`${currentLang === locale ? 'navbar__locale--active' : ''} navbar__locale`} 
							onClick={() => onLanguageChange(locale)} 
							disabled={currentLang === locale}
						>
							{locale}
						</button>
				</li>
					))}
			</ul>
		</nav>
	);

	return isMobile ? (
		<header className="mobile-header">
			<FontAwesomeIcon icon={faBars} onClick={() => setOpen(true)} />
			<div className={`mobile-header__sidebar ${isOpen && 'open'}`}>
				<FontAwesomeIcon icon={faTimes} className="mobile-header__sidebar__close" onClick={() => setOpen(false)} />
				<Nav />
			</div>
		</header>
	) : (
		<header className="header" style={{justifyContent: isHome ? 'flex-end': 'space-between'}}>
				{!isHome && <Link to={Routes.BASE} className="header__logo">
					<img src="../../assets/images/logo.png" />
				</Link>}
				<Nav />
		</header>
	);
};

export default Header;
