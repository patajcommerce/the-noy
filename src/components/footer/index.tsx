import * as React from 'react';
import { format } from 'date-fns';
import { useTranslation } from 'react-i18next';

import './index.scss';

// codebeat:disable[LOC]
export const Footer: React.FunctionComponent = () => {
	const { t } = useTranslation();

	return (
		<footer className="footer">
			<p>&copy; {format(new Date(), 'yyyy')}. {t('All rights reserved.')}</p>
			<p>{t("Thank you for visit me! Hope you'll enjoy your time here!")}</p>
		</footer>
	);
};
// codebeat:enable[LOC]

export default Footer;
