import * as React from 'react';
import { shallow } from 'enzyme';

import { Tooltip } from '.';

describe('Tooltip component', () => {
	it('should render successfully', () => {
		const tree = shallow(<Tooltip tooltipText="test text">Test content</Tooltip>);

		expect(tree).toMatchSnapshot();
	});
});
