import * as React from 'react';
import './index.scss';

interface Props {
	readonly children?: React.ReactNode | React.ReactNode[];
	readonly tooltipText: React.ReactNode | string;
}

export const Tooltip: React.FunctionComponent<Props> = ({ children, tooltipText }: Props) => (
	<div className="tooltip">
		{children}
		<span className="tooltip__text">{tooltipText}</span>
	</div>
);

export default Tooltip;
